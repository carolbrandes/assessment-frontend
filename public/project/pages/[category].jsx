import { useEffect, useContext, useState } from "react";
import { AppContext } from "../Store";
import { GET_CATEGORIES, GET_CATEGORY } from "../api-functions";
import { useRouter } from "next/dist/client/router";
import Head from "next/head";
import { BreadCrumb } from "../components/BreadCrumb/BreadCrumb";
import { Catalogo } from "../components/Catalogo/Catalogo";

export default function Category() {
  const { categories, setCategories } = useContext(AppContext);
  const router = useRouter();

  const categoryPath = router.asPath;
  const [category, setCategory] = useState({});
  const [products, setProducts] = useState([]);

  useEffect(() => {
    const getProducts = async (id) => setProducts(await GET_CATEGORY(id));

    const getCategory = () => {
      const categorypath = router.query.category;
      GET_CATEGORIES().then((res) => {
        setCategories(res)
        const categorySelected = categories.find(
          (category) => category.path === categorypath
        );

        setTimeout(() => {
          if (categorySelected !== undefined) {
            setCategory(categorySelected);
            getProducts(categorySelected.id);
          }
        }, 500);
      });
    };

    getCategory(getProducts);
  }, [categoryPath]);

  return (
    <>
     <Head>
        <title>Web Jump - {category.name}</title>
        <meta name="description" content={`Web Jump - ${category.name}`} />
        <meta name="keywords" content={`Web Jump - ${category.name}`} />
      </Head>
    <main className="container">
      <BreadCrumb categoryPath={categoryPath} category={category} />

      <Catalogo
        products={products.items}
        filters={products.filters}
        category={category}
        categories={categories}
      />
    </main>
    </>
  );
}
