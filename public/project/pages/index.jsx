import Head from "next/head";
import Link from "next/link";
import { AppContext } from "../Store";
import { useEffect, useContext } from "react";
import { GET_CATEGORIES } from "../api-functions";
import styles from "../styles/Home.module.scss";

export default function Home() {
  const { categories, setCategories } = useContext(AppContext);

  useEffect(() => {
    const getCategories = async () => setCategories(await GET_CATEGORIES());
    getCategories();
  }, []);

  return (
    <>
      <Head>
        <title>Web Jump</title>
        <meta name="description" content="Página inicial - Web Jump" />
        <meta name="keywords" content="Web Jump" />
      </Head>

      <main className="container">
        <div className="row justify-content-between pt-3">
       <nav className={`d-none d-md-block col-md-3 p-3 ${styles.menuLeft}`}>
         <ul>
           <Link href="/"><li className="pb-2">Página Inicial</li></Link>
            {categories !== undefined && categories.map(category => <Link key={category.id} href={category.path}><li className="pb-2">{category.name}</li></Link>)}
           <Link href="#"><li>Contato</li></Link>
         </ul>
       </nav>

       <section className="col-md-9">
         <div className={styles.banner}></div>

         <div className="pt-4">
           <h2 className={`pb-3 ${styles.title}`}>Seja Bem-vindo!</h2>

           <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas praesentium iure aperiam perspiciatis, incidunt veniam excepturi asperiores sint nulla fuga! Asperiores animi distinctio dolorum reprehenderit aut laboriosam provident illum porro.Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas praesentium iure aperiam perspiciatis, incidunt veniam excepturi asperiores sint nulla fuga! Asperiores animi distinctio dolorum reprehenderit aut laboriosam provident illum porro.Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas praesentium iure aperiam perspiciatis, incidunt veniam excepturi asperiores sint nulla fuga! Asperiores animi distinctio dolorum reprehenderit aut laboriosam provident illum porro.Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas praesentium iure aperiam perspiciatis, incidunt veniam excepturi asperiores sint nulla fuga! Asperiores animi distinctio dolorum reprehenderit aut laboriosam provident illum porro.</p>
         </div>
       </section>
        </div>
      </main>
    </>
  );
}
