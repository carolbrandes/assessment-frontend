import "bootstrap/dist/css/bootstrap.css";
import Store from "../Store";
import Head from "next/head";
import { Header } from "../components/Header/Header";
import "../styles/globals.scss";
import { Footer } from "../components/Footer/Footer";

function MyApp({ Component, pageProps }) {
  return (
    <>
     <Head>
        <link rel="shortcut icon" href="https://webjump.com.br/wp-content/uploads/2018/09/logo_webjump.png"/>
      </Head>
      <Store>
        <Header />
        <Component {...pageProps} />
        <Footer />
      </Store>
    </>
  );
}

export default MyApp;
