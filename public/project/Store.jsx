import React, { useState } from "react";

export const AppContext = React.createContext();

const Store = ({ children }) => {
  const [categories, setCategories] = useState([]);

  return (
    <AppContext.Provider value={{ categories, setCategories }}>
      {children}
    </AppContext.Provider>
  );
};

export default Store;
