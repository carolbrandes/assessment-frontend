import Link from "next/link";
import style from "./BreadCrumb.module.scss";

export const BreadCrumb = ({ categoryPath, category }) => {
  return (
    <nav className={`${style.breadcrumb} py-4`}>
      <Link href="/">
        <a>Página Inicial</a>
      </Link>
      <span className="mx-2">></span>
      <Link href={categoryPath}>
        <a>{category && category.name}</a>
      </Link>
    </nav>
  );
};
