import Link from "next/link";
import { useContext, useEffect, useState } from "react";
import { AppContext } from "../../Store";
import { GET_CATEGORIES } from "../../api-functions";
import { Search } from "../Search/Search";
import style from "./Header.module.scss";

export const Header = () => {
  const { categories, setCategories } = useContext(AppContext);
  const [menuMobile, setMenuMobile] = useState(false);
  const [searchMobile, setSearchMobile] = useState(false);

  useEffect(() => {
    const getCategories = async () => setCategories(await GET_CATEGORIES());
    getCategories();
  }, []);

  const handleMenu = () => {
    setMenuMobile(!menuMobile);
  };

  const handleSearch = () => {
    setSearchMobile(!searchMobile);
  };

  return (
    <header>
      <nav className={style.acessoUsuario}>
        <div className="container d-flex justify-content-end  py-2">
          <Link href="#">
            <a className="bold">Acesse sua Conta</a>
          </Link>
          <span className="px-2">ou</span>
          <Link href="#">
            <a className="bold">Cadastre-se</a>
          </Link>
        </div>
      </nav>

      <section className="container">
        <div className="row justify-content-between align-items-center py-5">
          <div className="col-2 d-block d-md-none">
            <img
              className="img-fluid cursor-pointer"
              src="/media/menu.png"
              alt="menu"
              onClick={handleMenu}
            />
          </div>

          <div className="col-6 col-md-4 col-lg-2">
            <Link href="/">
              <img
                className="img-fluid w-100"
                src="/media/logo.png"
                alt="WebJump"
              />
            </Link>
          </div>

          <div className="d-none d-md-block col-md-8 col-lg-6">
            <Search />
          </div>

          <div className="d-block d-md-none col-2">
            <img
              onClick={handleSearch}
              className="img-fluid cursor-pointer"
              src="/media/search.png"
              alt="buscar"
            />
          </div>
        </div>
      </section>

      <nav className={`${style.menu} ${menuMobile ? "fadeIn" : ""}`}>
        <div className="container d-flex flex-column flex-md-row  align-items-md-center py-3">
          <Link href="/">
            <a className="extra-bold py-4 py-md-0 me-md-5 pe-md-4">
              Página Inicial
            </a>
          </Link>
          {categories.length > 0 &&
            categories.map(({ id, name, path }) => (
              <Link key={id} href={path}>
                <a className="extra-bold pb-4 pb-md-0 me-5 pe-4">{name}</a>
              </Link>
            ))}
          <Link href="#">
            <a className="extra-bold pb-4 pb-md-0">Contato</a>
          </Link>
        </div>
      </nav>

      <div className={`${searchMobile ? "d-block" : "d-none"} d-md-none`}>
        <Search />
      </div>
    </header>
  );
};
