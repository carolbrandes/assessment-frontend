import style from "./Button.module.scss";

export const Button = ({ texto }) => {
  return <button className={style.button}>{texto}</button>;
};
