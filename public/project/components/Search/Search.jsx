import style from "./Search.module.scss";

export const Search = () => {
  return (
    <div className={style.search}>
      <div className="input-group mb-3 px-3 px-md-0">
        <input
          type="text"
          className="form-control"
          aria-label="Recipient's username"
          aria-describedby="button-addon2"
        />
        <button className="btn btn-primary bold" type="button" id="button-addon2">
          Buscar
        </button>
      </div>
    </div>
  );
};
