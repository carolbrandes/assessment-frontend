import style from "./Footer.module.scss"

export const Footer = () => <footer className={`container-lg mt-5 ${style.footer}`}></footer>