import { Button } from "../Button/Button";
import { formatterCurrency } from "../../functions";
import style from "./Catalogo.module.scss";
import { useState } from "react";
import Link from "next/link";
import { FaBorderAll, FaAlignLeft } from "react-icons/fa";

export const Catalogo = ({ products, category, filters, categories }) => {
  const [order, setOrder] = useState("menor");
  const [color, setColor] = useState("");
  const [gender, setGender] = useState("");
  const [grid, setGrid] = useState(true);
  const [row, setRow] = useState(false);

  const handleChange = (event) => setOrder(event.target.value);

  const handleColor = (event) => setColor(event.target.value);

  const handleGender = (event) => setGender(event.target.value);

  const handleGrid = () => {
    setGrid(true);
    setRow(false);
  };

  const handleRow = () => {
    setGrid(false);
    setRow(true);
  };

  const colors = () => {
    if (products !== undefined) {
      return new Set(products.map((p) => p.filter[0].color));
    }
  };

  const productsFiltered = (products) => {
    if (products !== undefined) {
      let productsFilter = products;
      if (color !== "") {
        productsFilter = products.filter(
          (product) => product.filter[0].color === color
        );
      }

      if (gender !== "") {
        productsFilter = products.filter(
          (product) => product.filter[0].gender === gender
        );
      }

      if (order === "menor") {
        return productsFilter.sort(
          (a, b) =>
            (a.specialPrice ? a.specialPrice : a.price) -
            (b.specialPrice ? b.specialPrice : b.price)
        );
      }

      if (order === "maior") {
        return productsFilter.sort(
          (a, b) =>
            (b.specialPrice ? b.specialPrice : b.price) -
            (a.specialPrice ? a.specialPrice : a.price)
        );
      }
    }
  };

  return (
    <section className="row justify-content-center justify-content-md-between">
      <nav className={`col-11 col-md-3 p-3 mb-4 mb-md-0 ${style.filterLeft}`}>
        <p className={`bold ps-2 ${style.tituloFiltro}`}>Filtre por</p>

        <div className="pb-2">
          <p className={`bold ps-2 mb-2 ${style.subtituloFiltro}`}>
            Categorias
          </p>
          <ul>
            {categories &&
              categories.map((category) => (
                <Link key={category.id} href={category.path}>
                  <li className={style.itensFiltro}>{category.name}</li>
                </Link>
              ))}
          </ul>
        </div>
        {filters !== undefined && filters[0].color && (
          <div>
            <p className={`bold ps-2 mb-2 ${style.subtituloFiltro}`}>
              {filters[0].color}
            </p>

            <div className="row">
              {Array.from(colors()).map((color) => (
                <div
                  key={color}
                  className="col-3 col-md-5 col-lg-4 col-xxl-3 form-check mb-3"
                  onChange={handleColor}
                >
                  <input
                    value={color}
                    className="d-none form-check-input"
                    type="radio"
                    name="flexRadioDefault"
                    id={color}
                  />
                  <label for={color}>
                    <div className={`labelColor ${color}`}></div>
                  </label>
                </div>
              ))}
            </div>
          </div>
        )}

        {filters !== undefined && filters[0].gender && (
          <div>
            <p className={`bold ps-2 mb-2 ${style.subtituloFiltro}`}>
              {filters[0].gender}
            </p>

            <div className="ps-2" onChange={handleGender}>
              <div className="form-check">
                <input
                  value="Feminina"
                  className="form-check-input"
                  type="radio"
                  name="flexRadioDefault"
                  id="flexRadioDefault1"
                />
                <label className="form-check-label" for="flexRadioDefault1">
                  Feminino
                </label>
              </div>

              <div className="form-check">
                <input
                  value="Masculina"
                  className="form-check-input"
                  type="radio"
                  name="flexRadioDefault"
                  id="flexRadioDefault1"
                />
                <label className="form-check-label" for="flexRadioDefault1">
                  Masculino
                </label>
              </div>
            </div>
          </div>
        )}
      </nav>

      <div className="col-11 col-md-8">
        <h2 className={style.titulo}>{category.name}</h2>

        <div
          className={`row justify-content-md-between  py-2 mb-4 ${style.filter}`}
        >
          <div className="d-none d-md-flex  align-items-center col-md-4">
            <span className="pe-2 cursor-pointer" onClick={handleGrid}>
              <FaBorderAll size={25} color={grid ? "#80bdb8" : "#9f9f9f"} />
            </span>
            <span className="cursor-pointer" onClick={handleRow}>
              <FaAlignLeft size={25} color={row ? "#80bdb8" : "#9f9f9f"} />
            </span>
          </div>

          <div className="col-12 col-md-6 px-md-0 d-flex align-items-center justify-content-md-end">
            <label className={`pe-2 bold ${style.label}`} htmlFor="ordenacao">
              Ordenar por
            </label>
            <select
              id="ordenacao"
              className="form-select w-50"
              aria-label="Default select example"
              value={order}
              onChange={handleChange}
            >
              <option value="menor">Menor Preço</option>
              <option value="maior">Maior Preço</option>
            </select>
          </div>
        </div>

        <div
          className={`row ${
            grid ? "flex-row" : "flex-column"
          } justify-content-evenly`}
        >
          {products &&
            products.length > 0 &&
            productsFiltered(products).map((product) => (
              <div
                className={` d-flex  align-items-center mb-5 ${
                  grid
                    ? "flex-column col-6 col-md-4 col-xxl-3"
                    : "flex-row col-12"
                } `}
                key={product.id}
              >
                <div className={`${style.image} ${grid ? "" : "col-4 me-4"}`}>
                  <img
                    className="img-fluid"
                    src={product.image}
                    alt={product.name}
                  />
                </div>

                <div>
                  <h3 className={`${style.name} pt-3 pb-4`}>{product.name}</h3>

                  {product.specialPrice ? (
                    <p>
                      <span className={`${style.oldPrice} pe-3`}>
                        {formatterCurrency(product.price)}
                      </span>{" "}
                      <span className={`${style.price} extra-bold`}>
                        {formatterCurrency(product.specialPrice)}
                      </span>
                    </p>
                  ) : (
                    <p>
                      <span className={`${style.price} extra-bold`}>
                        {formatterCurrency(product.price)}
                      </span>
                    </p>
                  )}

                  <Button texto="Comprar" />
                </div>
              </div>
            ))}
        </div>
      </div>
    </section>
  );
};
