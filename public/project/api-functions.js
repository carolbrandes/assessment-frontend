import axios from "axios";

const Api_baseUrl = "http://localhost:8888/api/V1/categories";

export const GET_CATEGORIES = async () => {
  const res = await axios.get(`${Api_baseUrl}/list`);
  return res.data.items;
};

export const GET_CATEGORY = async (id) => {
  const res = await axios.get(`${Api_baseUrl}/${id}`);
  console.log(res.data);
  return res.data;
};
