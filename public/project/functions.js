export const formatterCurrency = (number) => {
  return number.toLocaleString("pt-br", { style: "currency", currency: "BRL" });
};
